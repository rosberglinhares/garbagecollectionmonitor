# A monitor for JVM garbage collection.

This monitor expects a directory path as the first parameter. With this information, a scan is done
in this directory to automatically get the underlying servers. After that, a monitor is started for
each server gotten. Each monitor runs completely separated from each other, in its own thread.

The tool will assume that there always be a log file for each server in the current day.

## How to Run

To run the program with dummy log files, you can navigate to the root repository directory and execute:

```bash
python app.py ./data
```