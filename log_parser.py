#!/usr/bin/env python3

# MIT License
#
# Copyright (c) 2019 Rosberg Linhares (rosberglinhares@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
The parser for JVM garbage collection logs.
"""

import re

# 2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure)  10198M->1220M(10200M), 5.4572036 secs]
EVENT_FULL_GC_REGEX_PATTERN = r'(\S+): .*Full GC \(([\w ]+)\)\s+(\d+)M->(\d+)M.*, ([\d.]+) secs]'
# 2017-03-01T17:08:13.478-0500: 6686.150: [GC pause (G1 Evacuation Pause) (mixed) (to-space exhausted), 1.1669987 secs]
# 2017-03-01T17:08:26.441-0500: 6699.114: [GC pause (G1 Evacuation Pause) (young), 0.0429834 secs]
EVENT_PAUSE_REGEX_PATTERN = r'(\S+): .*GC pause \(([\w ]+)\) \(([\w ]+)\)(?: \(([\w -]+)\))?, ([\d.]+) secs]'
# 2017-03-01T15:17:04.162-0500: 16.834: [GC concurrent-root-region-scan-end, 0.0250209 secs]
EVENT_MISCELLANEOUS_REGEX_PATTERN = r'(\S+): .*?GC ([\w-]+).*, ([\d.]+) secs]'


class GarbageCollectorLogEvent:
    def __init__(self, date_time, duration):
        self.date_time = date_time
        self.duration = duration


class GarbageCollectorFullGcEvent(GarbageCollectorLogEvent):
    def __init__(self, date_time, duration, cause, memory_from, memory_to):
        super().__init__(date_time, duration)
        self.cause = cause
        self.memory_from = memory_from
        self.memory_to = memory_to


class GarbageCollectorPauseEvent(GarbageCollectorLogEvent):
    def __init__(self, date_time, duration, cause, action, collector):
        super().__init__(date_time, duration)
        self.cause = cause
        self.action = action
        self.collector = collector


class GarbageCollectorMiscellaneousEvent(GarbageCollectorLogEvent):
    def __init__(self, date_time, duration, event):
        super().__init__(date_time, duration)
        self.event = event


class GarbageCollectorLogParser:
    full_gc_regex = re.compile(EVENT_FULL_GC_REGEX_PATTERN)
    pause_regex = re.compile(EVENT_PAUSE_REGEX_PATTERN)
    miscellaneous_regex = re.compile(EVENT_MISCELLANEOUS_REGEX_PATTERN)
    
    @classmethod
    def parse_line(cls, log_line: str) -> GarbageCollectorLogEvent:
        match = cls.full_gc_regex.fullmatch(log_line)
        if match:
            date_time = match.group(1)
            cause = match.group(2)
            memory_from = int(match.group(3))
            memory_to = int(match.group(4))
            duration = float(match.group(5))
            return GarbageCollectorFullGcEvent(date_time, duration, cause, memory_from, memory_to)
        
        match = cls.pause_regex.fullmatch(log_line)
        if match:
            date_time = match.group(1)
            action = match.group(2)
            collector = match.group(3)
            # Cause is optional for the pause log event
            cause = match.group(4)
            duration = float(match.group(5))
            return GarbageCollectorPauseEvent(date_time, duration, cause, action, collector)
        
        match = cls.miscellaneous_regex.fullmatch(log_line)
        if match:
            date_time = match.group(1)
            event = match.group(2)
            duration = float(match.group(3))
            return GarbageCollectorMiscellaneousEvent(date_time, duration, event)
            
        return None