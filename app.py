#!/usr/bin/env python3

# MIT License
#
# Copyright (c) 2019 Rosberg Linhares (rosberglinhares@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
A monitor for JVM garbage collection.

This monitor expects a directory path as the first parameter. With this information, a scan is done
in this directory to automatically get the underlying servers. After that, a monitor is started for
each server gotten. Each monitor runs completely separated from each other, in its own thread.

The tool will assume that there always be a log file for each server in the current day.
"""

import sys
import os
import signal
import glob
import re
import logging
from threading import Event
from typing import Set
from gc_monitor import GarbageCollectorMonitor

# Log file name example: gc_2019-05-27_172.160.001.192.log
LOG_FILE_NAME_GLOB_PATTERN = '*.log'
LOG_FILE_NAME_REGEX_PATTERN = r'gc_\d{4}-\d{2}-\d{2}_((?:\d{1,3}\.){3}\d{1,3})\.log'
EXIT_EVENT_WAIT_TIMEOUT_IN_SECONDS = 1

# Flag to control when exit the program gracefully
exit_event = Event()

def main(logs_path: str) -> None:
    logging.basicConfig(format='%(asctime)s %(levelname)-7s %(message)s', level=logging.INFO,
                        datefmt='%Y-%m-%d %H:%M:%S')
    
    # Exit gracefully
    signal.signal(signal.SIGTERM, _sigterm_handler)
    signal.signal(signal.SIGINT, _sigterm_handler)
    
    print('JVM Garbage Collector Monitor')
    print('You can press Ctrl+C any moment to quit the program')
    print()
    
    servers = _get_servers_addresses_from_log_files(logs_path)
    gc_monitors = []
    
    # This variable is used for one node to check the status of others
    GarbageCollectorMonitor.all_monitors_in_cluster = gc_monitors;
    
    for server in servers:
        gc_monitor = GarbageCollectorMonitor(server, logs_path)
        gc_monitor.start()
        gc_monitors.append(gc_monitor)
    
    while not exit_event.isSet():
        # Must have a timeout, or else the _sigterm_handler is not executed
        exit_event.wait(EXIT_EVENT_WAIT_TIMEOUT_IN_SECONDS)
    
    # Wait for each monitor terminate possible pending tasks
    for gc_monitor in gc_monitors:
        gc_monitor.stop()
        gc_monitor.join()
    
    logging.shutdown()
    
def _sigterm_handler(signum, frame) -> None:
    global exit_event
    # Signal the main function to exit
    exit_event.set()

def _get_servers_addresses_from_log_files(logs_path: str) -> Set[str]:
    servers = set()
    log_file_name_regex = re.compile(LOG_FILE_NAME_REGEX_PATTERN)
    
    for full_log_file_path in glob.iglob(os.path.join(logs_path, LOG_FILE_NAME_GLOB_PATTERN)):
        log_file_path = os.path.basename(full_log_file_path)
        match = log_file_name_regex.fullmatch(log_file_path)
        if match:
            server = match.group(1)
            servers.add(server)
    
    return servers

if __name__ == "__main__":
    main(logs_path=sys.argv[1])