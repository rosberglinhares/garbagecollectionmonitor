#!/usr/bin/env python3

# MIT License
#
# Copyright (c) 2019 Rosberg Linhares (rosberglinhares@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
A JVM garbage collection monitor for a specific server.
"""

import os
import io
import logging
import json
from threading import Thread
from threading import Timer
from datetime import date
from datetime import datetime
from datetime import timedelta
from log_parser import GarbageCollectorLogParser
from log_parser import GarbageCollectorFullGcEvent
from log_parser import GarbageCollectorPauseEvent
from log_parser import GarbageCollectorMiscellaneousEvent

# Log file name example: gc_2019-05-27_172.160.001.192.log
LOG_FILE_NAME_FORMAT_PATTERN = 'gc_{}_{}.log'
LOG_PROCESS_INTERVAL_IN_SECONDS = 1
TIME_WINDOW_SIZE_IN_MINUTES = 5
MEMORY_BEING_FREED_MINIMUM_LIMIT = 0.2
MAX_EVENTS_MEMORY_BEING_FREED_IS_LOW = 10
FULL_GC_TIME_PERCENT_LIMIT = 0.15

class GarbageCollectorMonitor(Thread):
    # This variable is used for one node to check the status of others
    all_monitors_in_cluster = None
    
    def __init__(self, server_address: str, logs_path: str):
        super().__init__(daemon=True)
        # The server which this instance is monitoring
        self.__server_address = server_address
        # The directory path from which get the garbage collector server logs
        self.__logs_path = logs_path
        # An object pointing to the log file
        self.__log_file = None
        # The partial content from the last log line, when it has not been completely written yet
        # (without a newline char in the end)
        self.__log_line = ''
        # The start time of the time window
        self.__window_start_time = None
        # Number of FullGC events in which the memory being freed is below 20%
        self.__gc_memory_being_freed_low_count = 0
        # Total FullGC time during the time window
        self.__total_full_gc_time = timedelta()
        # A flag indicating if we have to stop this monitor
        self.__stop = False
    
    def run(self):
        logging.info(f'[{self.__server_address}] Monitor started')
        
        self.__open_current_day_log_file()
        
        # Already existing content before the monitor is alive should be discarded.
        self.__log_file.seek(0, io.SEEK_END)
        
        self.__window_start_time = datetime.now()
        
        while not self.__stop:
            timer = Timer(LOG_PROCESS_INTERVAL_IN_SECONDS, self.__search_for_log_content)
            timer.start()
            timer.join()
        
        self.__log_file.close()
        
        logging.info(f'[{self.__server_address}] Monitor terminated')
    
    def stop(self) -> None:
        self.__stop = True
    
    def __open_current_day_log_file(self) -> None:
        log_file_name = LOG_FILE_NAME_FORMAT_PATTERN.format(date.today().isoformat(), self.__server_address)
        log_file_path = os.path.join(self.__logs_path, log_file_name)
        self.__log_file = open(log_file_path, 'rt')
    
    def __search_for_log_content(self) -> None:
        line_complement = self.__log_file.readline()
        
        if line_complement != '':
            # New content found
            self.__log_line += line_complement
            if self.__log_line.endswith('\n'):
                # rstrip is necessary for discard log lines that contains only the new line character
                self.__log_line = self.__log_line.rstrip()
                if self.__log_line != '':
                    self.__process_log_line()
                self.__log_line = ''
        
        self.__check_time_window_elapsed()
        
    def __check_time_window_elapsed(self) -> None:
        time_window_elapsed = datetime.now() - self.__window_start_time
        time_window_elapsed_in_minutes = time_window_elapsed.seconds // 60
        
        if time_window_elapsed_in_minutes >= TIME_WINDOW_SIZE_IN_MINUTES:
            self.__start_new_time_window()
    
    def __start_new_time_window(self) -> None:
        logging.info(f'[{self.__server_address}] Starting a new time window')
        
        if datetime.now().day != self.__window_start_time.day:
            # Rotate the log file
            self.__log_file.close()
            self.__open_current_day_log_file()
        
        self.__log_line = ''
        self.__gc_memory_being_freed_low_count = 0
        self.__total_full_gc_time = timedelta()
        self.__window_start_time = datetime.now()
    
    def __process_log_line(self) -> None:
        gc_event = GarbageCollectorLogParser.parse_line(self.__log_line)
        
        Thread(target=self.__send_event_to_new_relic, args=(gc_event,)).start()
        
        if isinstance(gc_event, GarbageCollectorFullGcEvent):
            self.__process_full_gc_event(gc_event)
    
    def __send_event_to_new_relic(self, gc_event) -> None:
        # TODO: Implement a Producer-Consumer pattern
        
        json_obj = {}
        
        json_obj['date_time'] = gc_event.date_time
        json_obj['duration'] = gc_event.duration
        
        if isinstance(gc_event, GarbageCollectorFullGcEvent):
            json_obj['eventType'] = gc_event.cause
        elif isinstance(gc_event, GarbageCollectorPauseEvent):
            json_obj['eventType'] = gc_event.cause
            json_obj['action'] = gc_event.action
            json_obj['collector'] = gc_event.collector
        elif isinstance(gc_event, GarbageCollectorMiscellaneousEvent):
            json_obj['eventType'] = gc_event.event
        
        json_text = json.dumps([json_obj])
        
        print('Sending data to New Relic')
        print(json_text)
        
    def __process_full_gc_event(self, event_data: GarbageCollectorFullGcEvent) -> None:
        # 1. During a 5 min window, count the number of FullGC events in which the memory
        #    being freed is below 20% (that is, memory being freed is very low).
        
        memory_being_freed = 1 - event_data.memory_to / event_data.memory_from
        
        if memory_being_freed < MEMORY_BEING_FREED_MINIMUM_LIMIT:
            self.__gc_memory_being_freed_low_count += 1
        
        if self.__gc_memory_being_freed_low_count > MAX_EVENTS_MEMORY_BEING_FREED_IS_LOW:
            self.__restart_tomcat('Low memory being freed')
            self.__start_new_time_window()
            return
        
        
        # 2. If total FullGC time during a 5min window is greater than 15%
        
        full_gc_time = timedelta(seconds=event_data.duration)
        self.__total_full_gc_time += full_gc_time
        time_window_size_in_seconds = timedelta(minutes=TIME_WINDOW_SIZE_IN_MINUTES).seconds
        full_gc_time_percent = self.__total_full_gc_time.seconds / time_window_size_in_seconds
        
        if full_gc_time_percent > FULL_GC_TIME_PERCENT_LIMIT:
            self.__restart_tomcat('GC time is large')
            self.__start_new_time_window()
            return
    
    def __restart_tomcat(self, reason: str) -> None:
        # Restar tomcat server from machine with IP = self.__server_address
        
        for gc_monitor in self.__class__.all_monitors_in_cluster:
            if gc_monitor != self and not gc_monitor.is_node_alive():
                logging.info(f'[{self.__server_address}] It is not possible to restart the web server. Some other server is already out.')
                return
                
        logging.info(f'[{self.__server_address}] Restarting web server. Reason = {reason}')
        pass
        logging.info(f'[{self.__server_address}] Web server up')
        
    def is_node_alive(self) -> bool:
        # Do a request with timeout to self.__server_address to check if server is alive
        return True